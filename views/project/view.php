<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <?php $user = \app\models\Users::find()->where(['id' => $model->id])->one();?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'user_id',
            [
                'attribute' => 'user_id',
                'value' => $user->fio,
            ],
            'name',
            'cost',
            'date_start',
            'date_end',
        ],
    ]) ?>

      <br>  <?= Html::a('Назад в Список проектов', ['index', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
</div>
