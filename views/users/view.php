<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Пользователь : '. $model->fio;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];

?>
<div class="users-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fio',
            'login',
            'parol',
        ],
    ]) ?>

        <br><br>
        <?= Html::a('Назад в Список пользователей', ['index'], ['class' => 'btn btn-primary']) ?>
</div>

